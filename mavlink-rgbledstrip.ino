// Teensy 2.0 MAVLINK communication and LED driver

// Details on UART connection
// see: https://www.pjrc.com/teensy/td_uart.html

// To communicate between raspbery and teensy, connect GND pin to GPIO GND on raspberry
// Connect pin 7 (RX) to raspberry TX pin, and pin 8 (TX) to raspberry RX pin
// Both raspberry and teensy uart work on 3.3V, as well as the pixhawk telemetry ports
// Watch out with 5V TTL! While receiving might work, sending packets at 5V to the teensy might break it!

// To communicate with the teensy, run
// minicom -b 9600 -o -D /dev/ttyAMA0
// CTRL+A q to quit minicom

// Alternatively you can listen for serial output on the teensy USB port
// minicom -b 9600 -o -D /dev/ttyACM0

// Note: Before pushing the RESET button to bring the teensy in in program mode,
// DISCONNECT THE UART CABLES, otherwise it might not work, and you might have
// to restart the PC before it works again.


#include "mavlink/include/mavlink.h"

#ifdef USE_ADAFRUIT_NEOPIXEL
#include <Adafruit_NeoPixel.h>
#include <avr/power.h>
#else
#include "neopixel_fast.h"
#endif


// Uncomment this define to tunnel MAVlink messages from uart to USB (serial)
//#define MAVLINK_TUNNEL

// set this to the hardware serial port you wish to use
#define HWSERIAL Serial1

// Built-in status LED
const int status_led = LED_BUILTIN;

bool status_led_status = HIGH;

uint16_t packet_count = 0;


// NeoPixel setup
// Which pin on the Arduino is connected to the NeoPixels?
// On our Teensy 2.0 we use pin 0-6
#define PIN            0

#define PIXEL_PORT  PORTB  // Port of the pin the pixels are connected to
#define PIXEL_DDR   DDRB   // Port of the pin the pixels are connected to
#define PIXEL_BIT   PIN    // Bit of the pin the pixels are connected to


// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      60


void setup() {
  Serial.begin(9600);   // Setup USB serial
  HWSERIAL.begin(9600); // Setup UART (pin 7-8) serial
  pinMode(status_led, OUTPUT); // Enable built-in LED

  ledsetup();
}


uint16_t loop_count = 0;

void loop() {
  // Turn on Teensy status LED, then make it blink on receiving heartbeat packets
  digitalWrite(status_led, status_led_status);

  if ((loop_count % 5) == 0)
    send_heartbeat();

  drive_leds();

  //Serial.print("LOOP");
  //Serial.println(loop_count, DEC);
  loop_count++;
 
  delay(10);

  // TODO send heartbeat
  //HWSERIAL.write(buf, len);
  comm_receive();
}



// Aircraft status variables
bool mavlink_connected = false;
bool gps_fix = false;
float gps_hdop = 0.0;
bool is_armed = false;
bool is_ready = false;
uint8_t base_mode = 0;
unsigned char turn_orientation = 0;  // 0 is not turning, 1 is turning left, 2 is turning right
unsigned char battery_status = 100;  // Percentage between 0 and 100

uint16_t cycles_since_last_heartbeat = 0;

// in rad/s
// TODO tune this
#define YAWSPEED_THRESHOLD 0.05
// in radians
// TODO tune this
#define ROLL_THRESHOLD 0.1

// Set together with delay time
#define MAX_CYCLES_BETWEEN_HEARTBEATS 50  // Takes about 10 seconds to activate


// TODO if not received HEARTBEAT some time, go into failsafe mode, light status LED solidly again

void comm_receive() {
  // UART communication

  cycles_since_last_heartbeat++;
  
  mavlink_message_t msg;
  mavlink_status_t status;
  bool parsed_packet = false;
 
  while(HWSERIAL.available() > 0 && !parsed_packet)
  {
    uint8_t c = HWSERIAL.read();
#ifdef MAVLINK_TUNNEL
    Serial.write(c);  // Tunnel MAVlink stream through to USB
#endif

    /*
    // MAVlink activity indicator
    packet_count++;
    if (packet_count > 100) {
      packet_count = 0;
      toggle_led();
    }
    */

    // Try to get a new message
    if(mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status)) {
      parsed_packet = true;
#ifndef MAVLINK_TUNNEL
      //Serial.println("MAVlink packet parsed");
#endif
      // Handle message
      switch(msg.msgid)
      {
          case MAVLINK_MSG_ID_HEARTBEAT:
          {
            cycles_since_last_heartbeat = 0;
            mavlink_connected = true;
            
            // E.g. read GCS heartbeat and go into
            // comm lost mode if timer times out
            // TODO
 
            // isArmed(msg.base_mode)
            // isReady(msg.system_status)

            //Mode (arducoper armed/disarmed)
            base_mode = mavlink_msg_heartbeat_get_base_mode(&msg);
            is_armed = base_mode & MAV_MODE_FLAG_DECODE_POSITION_SAFETY;
            //is_armed = getBit(base_mode,7);

            is_ready = (base_mode != MAV_MODE_PREFLIGHT);
 
            // Heartbeat received, blink status LED
            toggle_led();
#ifndef MAVLINK_TUNNEL
            Serial.print("HEARTBEAT armed:");
            Serial.print(bool_to_str(is_armed));
            Serial.print(" ready:");
            Serial.println(bool_to_str(is_ready));
#endif
          }
          break;
        case MAVLINK_MSG_ID_SYS_STATUS:
          {
            // TODO this turns out to be battery A, try BATTERY_STATUS.battery_remaining
            battery_status = 100 - mavlink_msg_sys_status_get_battery_remaining(&msg);
#ifndef MAVLINK_TUNNEL
            Serial.print("SYS_STATUS battery:");
            Serial.println(battery_status);
#endif
          }
          break;
        case MAVLINK_MSG_ID_GPS_RAW_INT:
          {
            // 0-1: no fix, 2: 2D fix, 3: 3D fix
            gps_fix = (mavlink_msg_gps_raw_int_get_fix_type(&msg) > 1);
            //uint8_t osd_satellites_visible = mavlink_msg_gps_raw_int_get_satellites_visible(&msg);

            // TODO check whether hdop status is unknown? (If unknown, msg.eph is set to: UINT16_MAX)
            gps_hdop = mavlink_msg_gps_raw_int_get_eph(&msg);

#ifndef MAVLINK_TUNNEL
            Serial.print("GPS_RAW_INT gps_fix:");
            Serial.print(bool_to_str(gps_fix));
            Serial.print(" hdop:");
            Serial.println(gps_hdop);
#endif
          }
          break;
        case MAVLINK_MSG_ID_ATTITUDE:
          {
            turn_orientation = 0;
            
            // Angles in radians (3.14 radians == 180 degrees)
            // Range: -3.14 .. +3.14
            // Frame: right-handed, Z-down, X-front, Y-right (NED: North,East,Down)
            // Navigation frame: http://qgroundcontrol.org/coordinate_frame
            // https://erlerobotics.gitbooks.io/erlerobot/content/en/mavlink/ros/roscopter.html
            
            // Yaw speed: positive is right, negative left
            float yawspeed = mavlink_msg_attitude_get_yawspeed(&msg);
            if (fabs(yawspeed) > YAWSPEED_THRESHOLD) {
              if (yawspeed > 0)
                turn_orientation = 2; // Turning right
              else
                turn_orientation = 1; // Turning left
            }

            // Only use roll if no yaw is applied
            // TODO If yaw and roll combined, we could also decide which one to use based on pitch, roll and yawspeed
            float roll = mavlink_msg_attitude_get_roll(&msg);
            if (!turn_orientation) {
              // Roll: positive is bank right, negative is bank left
              if (fabs(roll) > ROLL_THRESHOLD) {
                if (roll > 0)
                  turn_orientation = 2; // Banking right
                else
                  turn_orientation = 1; // Banking left
              }
            }

#ifndef MAVLINK_TUNNEL
            Serial.print("ATTITUDE turn_orientation:");
            switch(turn_orientation) {
              case 1:
                Serial.print("left");
                break;
              case 2:
                Serial.print("right");
                break;
              default:
                Serial.print("none");
            }
            Serial.print(" yawspeed:");
            Serial.print(yawspeed);
            Serial.print(" roll:");
            Serial.print(roll);
            Serial.print(" (");
            Serial.print(bool_to_str(roll>0.0f)); Serial.print(", ");
            Serial.print(bool_to_str(yawspeed>0.0f)); Serial.print(", ");
            Serial.print(bool_to_str(fabs(yawspeed) > YAWSPEED_THRESHOLD)); Serial.print(", ");
            Serial.print(bool_to_str(fabs(roll) > ROLL_THRESHOLD));
            Serial.println(")");
#endif
          }
          break;
        default:
          //Do nothing
          break;
        }
    }

    if (cycles_since_last_heartbeat > MAX_CYCLES_BETWEEN_HEARTBEATS) {
      mavlink_connected = false; // TODO go into failsafe mode
      // Turn status led back on continuously to indicate the unit is still powered
      if (!status_led_status)
        toggle_led();
    }

  }
}

void send_heartbeat() {
  //mavlink_message_t msg;
  //uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  // TODO
}


void toggle_led() {
    status_led_status = !status_led_status;
    digitalWrite(status_led, status_led_status);
}

void drive_leds() {
  if (loop_count < 20)
    return;
  loop_count = 0;

  // TODO perhaps add a blinking led, like on the back of the DJI Inspire

  if (!gps_fix || !is_ready) {
    // Startup status

    // TODO if already ready but suddenly lost GPS fix, perhaps not return leds to all-green? Although perhaps some kind of indicator is very useful here
    // TODO perhaps we should account for hdop and/or nb of sattelites for a "bad gps signal" indicator

    cli();    // Disable all interrupts
    for( int p=0; p<NUMPIXELS; p++ ) {
      sendPixel( 150, 0, 0);  // All green
    }
    sei();    // Enable all interrupts

    show();
    return;
  }
  
  unsigned char color[3];
  if (status_led_status) {
    //color = pixels.Color(0,150,0); // Green
    color[0] = 10;
    color[1] = 60;
    color[2] = 25;
  } else {
    //color = pixels.Color(0,0,150); // Blue
    color[0] = 21;
    color[1] = 35;
    color[2] = 75;
  }

  /*
  // Make sure to disable interrupts when sending colors to LED string!
  */
  // TODO front blue or green, back red
  cli();    // Disable all interrupts
  for( int p=0; p<27; p++ ) {
    sendPixel(color[0], color[1], color[2]);
  }
  for( int p=27; p<NUMPIXELS-6; p++ ) {
    sendPixel(200, 0, 0);
  }
  

  // Turn indicators
  switch(turn_orientation)
  {
    case 1:
      // Left direction indicator
      for( int p=NUMPIXELS-6; p<NUMPIXELS-3; p++ ) {
        sendPixel(255, 255, 0);  // Yellow
      }
      for( int p=NUMPIXELS-3; p<NUMPIXELS; p++ ) {
        sendPixel(0, 0, 0);  // off
      }
      break;
    case 2:
      // Right direction indicator
      for( int p=NUMPIXELS-6; p<NUMPIXELS-3; p++ ) {
        sendPixel(0, 0, 0); // off
      }
      for( int p=NUMPIXELS-3; p<NUMPIXELS; p++ ) {
        sendPixel(255, 255, 0);  // Yellow
      }
      break;
    default: // 0
      // No direction indicator
      for( int p=NUMPIXELS-6; p<NUMPIXELS; p++) {
        sendPixel(0, 0, 0);  // all off
      }
      break;
  }
  sei();    // Enable all interrupts

  show();
}


// Display a single color on the whole string
void inline show_color( unsigned char r , unsigned char g , unsigned char b, int begin_idx, int end_idx) {
  for( int p=begin_idx; p<end_idx; p++ ) {
    sendPixel( r , g , b );
  }
  
}


/*
bool isArmed(uint16_t mode_flag) {
  return mode_flag & MAV_MODE_FLAG_DECODE_POSITION_SAFETY;
  // The safety switch status is indirectly reported through the MAV_SYS_STATUS_SENSOR message’s MOTOR_OUTPUTS bit.  It’s “0” if motors are disabled (i.e. switch is flashing) or “1” if motors are enabled (i.e. pwm outputs allows or system has no safety switch).
}

bool getBatteryStatus() {
  BATTERY_STATUS
}

bool getGpsStatus() {
  
}

bool isReady(uint16_t system_status) {
  // MAV_STATE

 
  //return system_status == MAV_STATE_ACTIVE;

  if (system_status == MAV_STATE_UNINIT)
    return false;
  if (system_status == MAV_STATE_BOOT)
    return false;
  if (system_status == MAV_STATE_CALIBRATING)
    return false;
  if (system_status == MAV_STATE_POWEROFF)
    // TODO perhaps power down leds, or fade out
    return false;

  return true;

  //MAV_STATE_STANDBY
  //MAV_STATE_CRITICAL
  //MAV_STATE_EMERGENCY
   
  // TODO perhaps MAV_STATE_CALIBRATING already signals whether there is GPS or not
}
*/

/*
void setup_power_saving() {
  // TODO
}
*/

// TODO check whether I can use mavlink_msg_heartbeat_get_custom_mode for enabling/disabling custom LED modes


inline char* bool_to_str(bool val) {
  if (val)
    return "yes";
  else
    return "no";
}
